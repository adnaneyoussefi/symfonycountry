<?php

namespace App\Data;

use App\Entity\Continent;

class SearchData {
    /**
     * @var Continent[]
     */
    public $continents = [];

    /**
     * @var string
     */
    public $recherche = '';
}